'use strict';
const Hapi = require('hapi');
const Glob = require('glob');
const Path = require('path');

const server = Hapi.server({
  port: 3000,
  host: 'localhost'
});

// bring your own validation function
const validate = async function(decoded, request) {
  // do your checks to see if the person is valid
};

const init = async () => {
  // include our module here ↓↓
  await server.register(require('hapi-auth-jwt2'));

  server.auth.strategy('jwt', 'jwt', {
    key: 'NeverShareYourSecret', // Never Share your secret key
    validate: validate, // validate function defined above
    verifyOptions: { algorithms: ['HS256'] } // pick a strong algorithm
  });

  server.auth.default('jwt');

  await Glob.sync('dist/controllers/*.js', {
    root: __dirname
  }).forEach(file => {
    const route = require(Path.join(__dirname, file));
    server.route(route);
  });

  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
