'use strict';

const Boom = require('boom');
const Models = require('@models');
const Joi = require('joi');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Bcrypt = require('bcrypt');

module.exports = {
  method: 'POST',
  path: '/users/authentication',
  handler: async ({ payload, pre }, h) => {

    return h
      .response({
        code: 200,
        data: pre.user
      })
      .code(200);
  },
  options: {
    auth: false,
    pre: [
      {
        assign: 'getUser',
        method: async ({ payload }, h) => {
          const user = await Models.user.findOne({
            where: {
              email: {
                [Op.eq]: payload.email
              }
            }
          });

          if (user === null) {
            return Boom.badData('Email or password is incorrect');
          }

          if (!Bcrypt.compareSync(payload.password, user.password)) {
            return Boom.unauthorized("Email or password is incorrect!");
          }

          return user;
        }
      }
    ],
    validate: {
      payload: Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required()
      })
    }
  }
};
