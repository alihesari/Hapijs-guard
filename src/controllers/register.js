'use strict';

const Boom = require('boom');
const Models = require('@models');
const Joi = require('joi');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Bcrypt = require('bcrypt');

module.exports = {
  method: 'POST',
  path: '/users/register',
  handler: async ({ payload, pre }, h) => {
    // Hash user password
    payload.password = await Bcrypt.hashSync(payload.password, 10);
    
    // Create user
    Models.user.create(payload);

    return h
      .response({
        code: 201,
        message: 'Register new user done.'
      })
      .code(201);
  },
  options: {
    auth: false,
    pre: [
      {
        assign: 'exist',
        method: async ({ payload }, h) => {
          const user = await Models.user.findOne({
            where: {
              email: {
                [Op.eq]: payload.email
              }
            }
          });

          if (user !== null) {
            return Boom.badData('Email is already use');
          }

          return true;
        }
      }
    ],
    validate: {
      payload: Joi.object({
        firstName: Joi.string()
          .max(255)
          .required(),
        lastName: Joi.string()
          .alphanum()
          .min(3)
          .max(255)
          .required(),
        email: Joi.string().required(),
        password: Joi.string()
          .min(6)
          .max(30)
          .required()
      })
    }
  }
};
