'use strict';
module.exports = (sequelize, DataTypes) => {
  var Permission = sequelize.define(
    'permission',
    {
      label: DataTypes.STRING
    },
    {}
  );
  Permission.associate = function(models) {
    // associations can be defined here
  };
  return Permission;
};
