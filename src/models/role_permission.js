'use strict';
module.exports = (sequelize, DataTypes) => {
  var role_permission = sequelize.define(
    'role_permission',
    {
      roleId: DataTypes.INTEGER,
      permissionId: DataTypes.INTEGER
    },
    {}
  );
  role_permission.associate = function(models) {
    // associations can be defined here
  };
  return role_permission;
};
