'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const config = require(__dirname + '/../config/config.json');
const mode = config['mode'];
const connectionConfig = config[mode];
const db = {};

const connection = new Sequelize(
  connectionConfig['database'],
  connectionConfig['username'],
  connectionConfig['password'],
  {
    dialect: connectionConfig['dialect'],
    operatorsAliases: Sequelize.Op
  }
);

fs.readdirSync(__dirname)
  .filter(file => {
    return (
      file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
    );
  })
  .forEach(file => {
    const model = connection['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = connection;

module.exports = db;
