Hapijs-guard is an open source management system Api built on Hapi and Mysql.

## Features
- [x] Registering users
- [ ] Authentication users
- [ ] Roles and permissions management
- [ ] Updating user account
- [ ] Deleting user account
- [ ] Changing a user account password
- [ ] Recovering user accounts
- [ ] Verifying user account

## Technology
Hapijs Guard is built with the Hapi framework. We're using Mysql as a data store.

## Requirements
- [Node.js](http://nodejs.org/download/) >= 8.x
- [Mysql](https://www.mysql.com/downloads/)

## Installation
```
$ git clone https://github.com/alihesari/Hapijs-guard.git
$ cd Hapijs-guard
$ npm install
```

