'use strict';

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fs = require('fs');
var path = require('path');
var Sequelize = require('sequelize');
var basename = path.basename(__filename);
var config = require(__dirname + '/../config/config.json');
var mode = config['mode'];
var connectionConfig = config[mode];
var db = {};

var connection = new Sequelize(connectionConfig['database'], connectionConfig['username'], connectionConfig['password'], {
  dialect: connectionConfig['dialect'],
  operatorsAliases: Sequelize.Op
});

fs.readdirSync(__dirname).filter(function (file) {
  return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js';
}).forEach(function (file) {
  var model = connection['import'](path.join(__dirname, file));
  db[model.name] = model;
});

(0, _keys2.default)(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = connection;

module.exports = db;