'use strict';

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Boom = require('boom');
var Models = require('../models');
var Joi = require('joi');
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
var Bcrypt = require('bcrypt');

module.exports = {
  method: 'POST',
  path: '/users/register',
  handler: function () {
    var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref, h) {
      var payload = _ref.payload,
          pre = _ref.pre;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return Bcrypt.hashSync(payload.password, 10);

            case 2:
              payload.password = _context.sent;


              // Create user
              Models.user.create(payload);

              return _context.abrupt('return', h.response({
                code: 201,
                message: 'Register new user done.'
              }).code(201));

            case 5:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    }));

    function handler(_x, _x2) {
      return _ref2.apply(this, arguments);
    }

    return handler;
  }(),
  options: {
    auth: false,
    pre: [{
      assign: 'exist',
      method: function () {
        var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(_ref3, h) {
          var payload = _ref3.payload;
          var user;
          return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _context2.next = 2;
                  return Models.user.findOne({
                    where: {
                      email: (0, _defineProperty3.default)({}, Op.eq, payload.email)
                    }
                  });

                case 2:
                  user = _context2.sent;

                  if (!(user !== null)) {
                    _context2.next = 5;
                    break;
                  }

                  return _context2.abrupt('return', Boom.badData('Email is already use'));

                case 5:
                  return _context2.abrupt('return', true);

                case 6:
                case 'end':
                  return _context2.stop();
              }
            }
          }, _callee2, undefined);
        }));

        function method(_x3, _x4) {
          return _ref4.apply(this, arguments);
        }

        return method;
      }()
    }],
    validate: {
      payload: Joi.object({
        firstName: Joi.string().max(255).required(),
        lastName: Joi.string().alphanum().min(3).max(255).required(),
        email: Joi.string().required(),
        password: Joi.string().min(6).max(30).required()
      })
    }
  }
};